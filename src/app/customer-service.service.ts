import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Customer} from './customer';
import {Location} from '@angular/common';


const httpOptions = {
  headers:new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class CustomerServiceService {
  private url="http://localhost:8080/customers";



 constructor(private http:HttpClient,private location:Location) { }

 getCustomers():Observable<Customer[]>{
   return this.http.get<Customer[]>(this.url);
 }

 getCustomer(id:number):Observable<Customer>{
   const url=`${this.url}/${id}`;
   return this.http.get<Customer>(url);
 }

 addCustomer (customer:Customer): void{
   this.http.post<Customer>(this.url,customer,httpOptions)
   .subscribe(()=> this.goBack());
 }

  updateCustomer (id:number,customer: Customer): Observable<Customer> {
   const url1=`${this.url}/${id}`;
   return this.http.put<Customer>(url1,customer,httpOptions);
  }

  deleteCustomer (customer: Customer | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${this.url}/${id}`;

    return this.http.delete<Customer>(url, httpOptions);
  }

 goBack():void{
   this.location.back();
 }


}
