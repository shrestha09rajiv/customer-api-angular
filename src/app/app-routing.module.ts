import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import {EditCustomerComponent} from "./edit-customer/edit-customer.component";

const routes: Routes = [

{path:'customer',component:CustomerComponent},
{path:'',redirectTo:'/customer', pathMatch:'full'},
{path:'add-customer',component:AddCustomerComponent},
  {path:'update-customer/:id',component:EditCustomerComponent},
  {path:'delete-customer/:id',component:CustomerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
