import { Component, OnInit } from '@angular/core';
import { CustomerServiceService } from '../customer-service.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  constructor(private customerservice:CustomerServiceService) { }

  ngOnInit() {
  }

  addCustomer(firstName:String,lastName:String): void {
    
    this.customerservice.addCustomer({firstName, lastName} as Customer);
  }

}
