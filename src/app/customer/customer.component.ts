import { Component, OnInit } from '@angular/core';
import { CustomerServiceService } from '../customer-service.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customers:Customer[];
  constructor(private customerservice:CustomerServiceService) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers():void{
    this.customerservice.getCustomers().subscribe(customers=>this.customers=customers);
  }

  deleteCustomer(customer: Customer): void {
    this.customers = this.customers.filter(c => c !== customer);
    this.customerservice.deleteCustomer(customer).subscribe();
  }



}
