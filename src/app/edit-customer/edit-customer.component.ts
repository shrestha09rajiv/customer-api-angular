import {Component, Input, OnInit} from '@angular/core';
import {CustomerServiceService} from "../customer-service.service";
import {Customer} from "../customer";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {

  @Input() customer:Customer;

  constructor(private customerservice:CustomerServiceService,
              private route: ActivatedRoute,
              private location:Location) { }

  ngOnInit() {
    this.getCustomer();
  }

  getCustomer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.customerservice.getCustomer(id)
      .subscribe(customer => this.customer = customer);
  }

  goBack(): void {
    this.location.back();
  }
  updateCustomer():void{
    this.customerservice.updateCustomer(this.customer.id,this.customer)
      .subscribe(() => this.goBack());
  }



}
